$(function($) {

    affixsidebar()
	
	$(window).on("orientationchange", function() {
		affixsidebar()
	})

	$(window).resize(function() {
		affixsidebar()
	})

    $('.select-00').selectmenu({
	  width: '100%'
	})

	$('.header-search-link').on('click', function() {
		$('.header-search-popup').show('fast')
		return false
	})

	$(document).click( function(event){
		if( $(event.target).closest(".header-search-popup").length ) 
			return
		$(".header-search-popup").hide("fast")
			event.stopPropagation()
	})

	// mobal menu
	$('.btn-menu').on('click', function() {
	    if ( $(this).hasClass('btn-menu-active') ) {
	      $(this).removeClass('btn-menu-active');
	      $('.global-menu').removeClass('active')
	      $('.overlay-menu').css({display: 'none' });
	      $('html').css('overflow','auto');
	    } else {
	      $(this).addClass('btn-menu-active');
	      $('.global-menu').addClass('active')
	      $('.overlay-menu').css({display: 'block' });
	      $('html').css('overflow','hidden');
	    }
	    return false;
	})

	$('.overlay-menu').on('click', function() {
	    $('.global-menu').removeClass('active')
	    $('.overlay-menu').css({display: 'none' });
	    $('.btn-menu').removeClass('btn-menu-active');
	    $('html').css('overflow','auto');
	})

	$(".global-menu").on("swiperight",function(){
	    $('.btn-menu').removeClass('btn-menu-active');
	    $('.global-menu').removeClass('active')
	    $('.overlay-menu').css({display: 'none' });
	    $('html').css('overflow','auto');
	})

	var swiper = new Swiper('.gallery', {
        pagination: '.gallery .swiper-pagination',
        nextButton: '.gallery .swiper-button-next',
        prevButton: '.gallery .swiper-button-prev',
        centeredSlides: true,
       	slidesPerView: 'auto',
        paginationClickable: true,
        spaceBetween: 0,
        loop: true
    })
    
	var swiper = new Swiper('.reviews', {
        nextButton: '.reviews .swiper-button-next',
        prevButton: '.reviews .swiper-button-prev',
        spaceBetween: 40,
        slidesPerView: 3,
        paginationClickable: true,
        breakpoints: {
		    992: {
		      slidesPerView: 1
		    },
		    1200: {
		      slidesPerView: 2
		    }
		}
    })
    
    var swiper = new Swiper('.gallery-mini', {
        pagination: '.gallery-mini .swiper-pagination',
        nextButton: '.gallery-mini .swiper-button-next',
        prevButton: '.gallery-mini .swiper-button-prev',
        spaceBetween: 20,
        slidesPerView: 4,
        paginationClickable: true,
        breakpoints: {
            768: {
              slidesPerView: 1
            },
            1200: {
              slidesPerView: 3
            }
        }
    })
    
	var swiper = new Swiper('.gallery-photo', {
        nextButton: '.gallery-photo .swiper-button-next',
        prevButton: '.gallery-photo .swiper-button-prev',
        spaceBetween: 20,
        centeredSlides: true,
        slidesPerView: 'auto',
        paginationClickable: true,
        loop: true
    })

	var swiper = new Swiper('.header-slider', {
        slidesPerView: 1,
        effect: 'fade',
        autoplay: 3000,
        paginationClickable: true,
        autoplayDisableOnInteraction: false        
    })

    var maskList = $.masksSort($.masksLoad("js/data/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            // clearIncomplete: true,
            showMaskOnHover: false,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskList,
        listKey: "mask",
        onMaskChange: function (maskObj, completed) {
            if (completed && typeof maskObj.cc == 'string') {
                $(this).css({'background-image': 'url(\'js/flag/' + maskObj.cc.toLowerCase() + '.png\')'})
            } else if (completed && typeof maskObj.cc == 'object' && typeof maskObj.cc[0] == 'string') {
                $(this).css({'background-image': 'url(\'js/flag/' + maskObj.cc[0].toLowerCase() + '.png\')'})
            } else {
                $(this).css({'background-image': 'url(\'js/flag/ru.png\')'})
            }
            $(this).attr("placeholder", '+_(___)___-__-__')
        }
    }

    $('.mask-phone').inputmask("+9(999)999-99-99");

    $('.phoneflag').inputmasks(maskOpts)

    $('.link-morejs').on('click', function() {
    	$(this).toggleClass('active')
    	$('.bblock-slide').slideToggle('fast')
    	$('.gallery-about-slide').slideToggle('fast')
    	return false
    })

    $('.filter-side-title').on('click', function() {
    	$(this).toggleClass('active')
    	$(this).parent('.filter-side-item').toggleClass('active')
    	return false
    })

    $('.filter-side-linkslide').on('click', function() {
    	$(this).toggleClass('active')
    	$('.filter-side-bodyslide').slideToggle('fast')
    	return false
    })

    $("#map-continents").CSSMap({
      "size": 1280
    })

    if ( $(window).width() <=992 ) {
        $('.li-catalog > a').on('click', function() {
            $(this).parents('li').toggleClass('active')
            $(this).next('.header-menu-podmenu').slideToggle('fast')
        })
    }

    $('.toggleModalVideoYoutube').click(function () {
        var src = $(this).data('videosrc') + '?autoplay=1';
        $('#revideo').modal('show')
        $('#revideo iframe').attr('src', src)
        return false
    })

    $('#revideo').on('hidden.bs.modal', function () {
        $('#revideo iframe').removeAttr('src')
    })

       

})

function affixsidebar() {
    if ( $(window).height() > $('.side-form-listroutes').outerHeight() + 30 ) {
        $('.side-form-listroutes').affix({
            offset: {
              top: $('header').outerHeight(true),
              bottom: $('footer').outerHeight(true) + 75
            }
        })
    } 
}